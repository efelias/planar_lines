#include "my_h5.h"
#include <iostream>
#include <vector>
#include <iterator>
#include <H5Cpp.h>
#include <chrono>
#include <ctime>
#include <unordered_map>

using namespace H5;
using namespace std;

string getTimeStr(){
    auto now = chrono::system_clock::to_time_t(chrono::system_clock::now());
    string s(30, '\0');
    strftime(&s[0], s.size(), "%Y-%m-%d %H:%M:%S", localtime(&now));
    return s;
}


void Attr(Group * h5file, const string &name, const string val){
    DataSpace attr_dataspace = DataSpace(H5S_SCALAR);
    StrType strdatatype(PredType::C_S1, 256); // of length 256 characters
    //const H5std_string strwritebuf(val);
    Attribute myatt_in = h5file->createAttribute( name, strdatatype, attr_dataspace);
    myatt_in.write(strdatatype, val);
    myatt_in.close();
}

void Attr(Group * h5file, const string & name, const int val){
    DataSpace attr_dataspace = DataSpace(H5S_SCALAR);

    Attribute myatt_in = h5file->createAttribute(name, PredType::NATIVE_INT, attr_dataspace);
    myatt_in.write(PredType::NATIVE_INT, &val);
    myatt_in.close();
}

void Attr(Group * h5file, const string &name, const float val){
    DataSpace attr_dataspace = DataSpace(H5S_SCALAR);

    Attribute myatt_in = h5file->createAttribute(name, PredType::NATIVE_FLOAT, attr_dataspace);
    myatt_in.write(PredType::NATIVE_FLOAT, &val);
    myatt_in.close();
}


void Attr(Group * h5file, const string &name, const double val){
    DataSpace attr_dataspace = DataSpace(H5S_SCALAR);

    Attribute myatt_in = h5file->createAttribute(name, PredType::NATIVE_DOUBLE, attr_dataspace);
    myatt_in.write(PredType::NATIVE_DOUBLE, &val);
    myatt_in.close();
}

void my_h5::open_file()
{
  if (!file){
      try {
          Exception::dontPrint();
          file = new H5File(file_name, H5F_ACC_EXCL);
      } catch (H5::FileIException e) {
          Exception::dontPrint();
          file = new H5File(file_name, H5F_ACC_RDWR);
      }
    }
}

void my_h5::open_group()
{
  if (!file){
      open_file();
    }
  if (!is_open){
      group_name = getTimeStr();
      group = file->createGroup(group_name);
      is_open = true;
    }
}

void my_h5::new_group()
{
  if(!is_open){
      open_group();
    }else{
      group.close();
      is_open = false;
      open_group();
    }
}

void my_h5::add_data(const string &name, thrust::host_vector<int> &v, bool new_g)
{
  if(!is_open){
      open_group();
    }else{
      if (new_g){
          new_group();
        }
    }
  size_t size = v.size();
  hsize_t dimsf[] = {size};
  DataSpace dataspace(1, dimsf);

  DataType datatype(PredType::NATIVE_INT);

  DataSet dataset = group.createDataSet( name, datatype, dataspace);
  dataset.write(&v[0], datatype);
  dataset.close();
  dataspace.close();
}

void my_h5::add_data(const string &name, thrust::host_vector<float> &v, bool new_g)
{
  if(!is_open){
      open_group();
    }else{
      if (new_g){
          new_group();
        }
    }
  size_t size = v.size();
  hsize_t dimsf[] = {size};
  DataSpace dataspace(1, dimsf);

  DataType datatype(PredType::NATIVE_FLOAT);

  DataSet dataset = group.createDataSet( name, datatype, dataspace);
  dataset.write(&v[0], datatype);
  dataset.close();
  dataspace.close();
}


void my_h5::add_data(const string &name, thrust::host_vector<double> &v, bool new_g)
{
  if(!is_open){
      open_group();
    }else{
      if (new_g){
          new_group();
        }
    }
  size_t size = v.size();
  hsize_t dimsf[] = {size};
  DataSpace dataspace(1, dimsf);

  DataType datatype(PredType::NATIVE_DOUBLE);

  DataSet dataset = group.createDataSet( name, datatype, dataspace);
  dataset.write(&v[0], datatype);
  dataset.close();
  dataspace.close();
}

void my_h5::close_file()
{
  if(is_open){
      group.close();
      is_open = false;
    }
  if(file){
      file->close();
      file = nullptr;
  }
}

my_h5::~my_h5()
{
  if(is_open){
      group.close();
      is_open = false;
    }
  if(file){
      file->close();
      file = nullptr;
    }
}


void my_h5::set_atribute(const string &name,const string val, bool new_g)
{
  if(!is_open){
      open_group();
    }else{
      if (new_g){
          new_group();
        }
    }
  Attr(&group, name, val);
}

void my_h5::set_atribute(const string &name,const int val, bool new_g)
{
  if(!is_open){
      open_group();
    }else{
      if (new_g){
          new_group();
        }
    }
  Attr(&group, name, val);
}

void my_h5::set_atribute(const string &name,const float val, bool new_g)
{
  if(!is_open){
      open_group();
    }else{
      if (new_g){
          new_group();
        }
    }
  Attr(&group, name, val);
}

void my_h5::set_atribute(const string &name,const double val, bool new_g)
{
  if(!is_open){
      open_group();
    }else{
      if (new_g){
          new_group();
        }
    }
  Attr(&group, name, val);
}

void my_h5::read_data(const string &group_name, const string &name, thrust::host_vector<float> &v)
{
  if (!file){
      open_file();
    }
  group = file->openGroup(group_name);
  DataSet dt = group.openDataSet(name);
  DataSpace ds = dt.getSpace();
  hsize_t rank;
  hsize_t dims[2];
  rank = ds.getSimpleExtentDims(dims);
  cout <<"Dataset : " << name << ", rank : " << rank << ", dims : {" << dims[0] << ", " << dims[1] << "}" << endl;
  if ( v.size() != dims[0]){
      v.resize(dims[0]);
    }
  dt.read(&v[0], PredType::NATIVE_FLOAT);
  dt.close();
  group.close();
}

void my_h5::read_data(const string &group_name, const string &name, thrust::host_vector<int> &v)
{
  if (!file){
      open_file();
    }
  group = file->openGroup(group_name);
  DataSet dt = group.openDataSet(name);
  DataSpace ds = dt.getSpace();
  hsize_t rank;
  hsize_t dims[2];
  rank = ds.getSimpleExtentDims(dims);
  cout <<"Dataset : " << name << ", rank : " << rank << ", dims : {" << dims[0] << ", " << dims[1] << "}" << endl;
  if ( v.size() != dims[0]){
      v.resize(dims[0]);
    }
  dt.read(&v[0], PredType::NATIVE_INT);
  dt.close();
  group.close();
}

void my_h5::read_data(const string &group_name, const string &name, thrust::host_vector<double> &v)
{
  if (!file){
      open_file();
    }
  group = file->openGroup(group_name);
  DataSet dt = group.openDataSet(name);
  DataSpace ds = dt.getSpace();
  hsize_t rank;
  hsize_t dims[2];
  rank = ds.getSimpleExtentDims(dims);
  cout <<"Dataset : " << name << ", rank : " << rank << ", dims : {" << dims[0] << ", " << dims[1] << "}" << endl;
  if ( v.size() != dims[0]){
      v.resize(dims[0]);
    }
  dt.read(&v[0], PredType::NATIVE_DOUBLE);
  dt.close();
  group.close();
}


void my_h5::read_attribute(const string &group_name, const string &name, int & val)
{
  if (!file){
      open_file();
    }
  group = file->openGroup(group_name);
  Attribute at = group.openAttribute(name);
  at.read(PredType::NATIVE_INT, &val);
}

void my_h5::read_attribute(const string &group_name, const string &name, float & val)
{
  if (!file){
      open_file();
    }
  group = file->openGroup(group_name);
  Attribute at = group.openAttribute(name);
  at.read(PredType::NATIVE_FLOAT, &val);
}

void my_h5::read_attribute(const string &group_name, const string &name, double & val)
{
  if (!file){
      open_file();
    }
  group = file->openGroup(group_name);
  Attribute at = group.openAttribute(name);
  at.read(PredType::NATIVE_DOUBLE, &val);
}

queue<string> my_h5::print_objets()
{
  string pStr;
  queue<string> a;

  if (!file){
      open_file();
    }
  for( int i = 0; i < file->getNumObjs(); i++ )
  {

      file->getObjnameByIdx(i, pStr, 128 );
      cout << "Objet number " << i << " : " << pStr << endl;
      a.push(pStr);
  }
  return a;
}

void my_h5::read_group(const string & name)
{
  if (!file){
      open_file();
    }
  group = file->openGroup(name);
  Attribute at;
  DataSet dt;
  string s;
  for (int i = 0; i < group.getNumAttrs(); ++i) {
      //group.getObjnameByIdx(i, s, 128);
      at = group.openAttribute(i);
      s = at.getName(128);
      cout << "objet in group " << name << " number " << i << " : " << s << endl;
      switch (at.getTypeClass()){
        case H5T_INTEGER:
          cout << "integer" << endl;
          break;
        case H5T_FLOAT:
          cout << "float" << endl;
          break;
        case H5T_STRING:
          cout << "string" << endl;
          break;
        }
    }
  for (int i = 0; i < group.getNumObjs(); ++i) {
      s = group.getObjnameByIdx(i);
      dt = group.openDataSet(s);
      cout << "dataset in group " << name << "number " << i << " : " << s << endl;
      switch (dt.getTypeClass()){
        case H5T_INTEGER:
          cout << "integer" << endl;
          break;
        case H5T_FLOAT:
          cout << "float" << endl;
          break;
        case H5T_STRING:
          cout << "string" << endl;
          break;
        }

      cout << "space mem : " << dt.getStorageSize() << endl;
    }
  dt.close();
  group.close();
}

std::string my_h5::get_last_obj(int index)
{
  string pStr;
  if (!file){
      open_file();
    }
  if (index == -1){
      int n_last =  file->getNumObjs();
      file->getObjnameByIdx(n_last-1, pStr, 128);
  }else{
     file->getObjnameByIdx(index, pStr, 128);
  }

  return pStr;
}

