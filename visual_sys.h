#ifndef VISUAL_SYS_H
#define VISUAL_SYS_H
#include "posicioner.h"
#include "vortex.h"
#include "functors.h"

class  visual_sys :
    public posicioner,
    public vortex
{

public:
  visual_sys(Parms init_parms, init_mode mode = init_mode::ZIGZAG);
  void update_positions_and_colors();
};

#endif // VISUAL_SYS_H
