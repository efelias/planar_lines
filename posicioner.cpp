#include "posicioner.h"

#ifndef CPU
void registerGLBufferObject(uint vbo, struct cudaGraphicsResource **cuda_vbo_resource)
{
    cudaGraphicsGLRegisterBuffer(cuda_vbo_resource, vbo, cudaGraphicsMapFlagsNone);
}

void unregisterGLBufferObject(struct cudaGraphicsResource *cuda_vbo_resource)
{
    cudaGraphicsUnregisterResource(cuda_vbo_resource);
}

void *mapGLBufferObject(struct cudaGraphicsResource **cuda_vbo_resource)
{
    void *ptr;
    cudaGraphicsMapResources(1, cuda_vbo_resource, 0);
    size_t num_bytes;
    cudaGraphicsResourceGetMappedPointer((void **)&ptr, &num_bytes, *cuda_vbo_resource);
    return ptr;
}

void unmapGLBufferObject(struct cudaGraphicsResource *cuda_vbo_resource)
{
    cudaGraphicsUnmapResources(1, &cuda_vbo_resource, 0);
}
#endif

posicioner::posicioner(int N) :
    posVbo(0),
    colorVBO(0),
    buffer_size(4*N),
    current_part_num(N)
{
    _init_openGL();
}


posicioner::~posicioner()
{
    _finalize_openGL();
}


uint posicioner::createVBO(uint size)
{
  GLuint vbo;
  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, size, 0, GL_DYNAMIC_DRAW);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  return vbo;
}

void posicioner::_init_openGL()
{
  posVbo = createVBO(buffer_size*sizeof(float));
  colorVBO = createVBO(buffer_size*sizeof(float));
#ifndef CPU
  registerGLBufferObject(posVbo, &m_cuda_posvbo_resource);
  registerGLBufferObject(colorVBO, &m_cuda_colorvbo_resource);
#endif
}

void posicioner::_finalize_openGL()
{
#ifndef CPU
  unregisterGLBufferObject(m_cuda_colorvbo_resource);
  unregisterGLBufferObject(m_cuda_posvbo_resource);
#endif
  glDeleteBuffers(1, (const GLuint *)&posVbo);
  glDeleteBuffers(1, (const GLuint *)&colorVBO);
}
