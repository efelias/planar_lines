import h5py as h5
import numpy as np
import matplotlib.pyplot as plt
import sys

def get_line(x, n, Lx, Ly):
    line = []
    for i in range(Ly):
        line.append(x[n+i*Lx])
    return np.array(line)

def get_line_u(x, n, a0, Lx, Ly):
    line = []
    for i in range(Ly):
        line.append(x[n+i*Lx]-n*a0)
    return np.array(line)

f = h5.File(sys.argv[1], 'r')

k = list(f.keys())
g = f[k[-1]]

a0 = g.attrs['a0']
N = g.attrs['N']
Lx = int(g.attrs['Lx']/a0)
Ly = int(g.attrs['Ly']/a0)

x, y = g['X'][...], g['Y'][...]

U = np.array([ get_line_u(x, i, a0, Lx, Ly) for i in range(Lx)])
lines = np.array([ get_line(x, i, Lx, Ly) for i in range(Lx)])
#
for i in lines: plt.plot(i)
plt.savefig('lines')
#
plt.clf()
#
w = np.array([np.mean((np.roll(U, n, axis=0)-U)**2) for n in range(Lx//2)])
##w = np.array([np.sum((np.roll(U, n, axis=0)*U), axis=0)/L for n in range(L//2)])
xx = np.arange(Lx//2)*a0
#
L = Lx
print(w.shape)
plt.loglog(xx, w, '.-')
a = 5e-3
#
##plt.loglog(xx, a*xx**2, 'k--')
##plt.loglog(xx, a*xx, 'k-.')
#
#plt.show()
#
#sq = np.array([np.abs(np.fft.fft(i)**2)/Ly for i in U])
#
#plt.clf()
#
#xx = 2*np.pi*np.arange(1, Ly//2+1)/Ly
#
#for i in sq: 
#    plt.loglog(xx/(2*np.pi/5), i[1:Ly//2+1])
#plt.loglog(xx/(2*np.pi), a/(xx/(2*np.pi))**2, 'k--')
#plt.loglog(xx/(2*np.pi), a/(xx/(2*np.pi)), 'k-.')
#
#plt.show()
#
#sq = np.array([np.abs(np.fft.fft(i)**2)/L for i in U.T])
#
#plt.clf()
#
#a = 1e-1
#xx = 2*np.pi*np.arange(1, L//2+1)/L
#for i in sq: 
#    plt.loglog(xx/(2*np.pi/5), i[1:L//2+1])
#plt.loglog(xx/(2*np.pi/a0), a/(xx/(2*np.pi/a0))**2, 'k--')
#plt.loglog(xx/(2*np.pi/a0), a/(xx/(2*np.pi/a0)), 'k-.')
#
#plt.show()


for j in k:
    g = f[j]
    a0 = g.attrs['a0']
    N = g.attrs['N']
    Lx = int(g.attrs['Lx']/a0)
    Ly = int(g.attrs['Ly']/a0)

    x, y = g['X'][...], g['Y'][...]
    U = np.array([ get_line_u(x, i, a0, Lx, Ly) for i in range(Lx)])
    sq = np.array([np.abs(np.fft.fft(i)**2)/Ly for i in U])
    xx = 2*np.pi*np.arange(1, Ly//2+1)/Ly

    plt.loglog(xx/(2*np.pi/5), xx**2*sq.mean(axis=0)[1:Ly//2+1])
plt.loglog(xx/(2*np.pi), a/(xx/(2*np.pi))**2, 'k--')
plt.loglog(xx/(2*np.pi), a/(xx/(2*np.pi)), 'k-.')

plt.show()

plt.clf()


for j in k:
    g = f[j]
    a0 = g.attrs['a0']
    N = g.attrs['N']
    Lx = int(g.attrs['Lx']/a0)
    Ly = int(g.attrs['Ly']/a0)
    x, y = g['X'][...], g['Y'][...]
    xx = 2*np.pi*np.arange(1, Lx//2+1)/Lx
    U = np.array([ get_line_u(x, i, a0, Lx, Ly) for i in range(Lx)])
    sq = np.array([np.abs(np.fft.fft(i)**2)/Ly for i in U.T])
    plt.loglog(xx/(2*np.pi/5), xx**2*sq.mean(axis=0)[1:Lx//2+1])
plt.loglog(xx/(2*np.pi/a0), a/(xx/(2*np.pi/a0))**2, 'k--')
plt.loglog(xx/(2*np.pi/a0), a/(xx/(2*np.pi/a0)), 'k-.')

plt.show()
