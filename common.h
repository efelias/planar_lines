#include <stdio.h>
#include <cmath>

#include <thrust/device_vector.h>
#include "Random123/philox.h"
#include "Random123/u01.h"

#define  CONS 0.26666666666666666667
#define PI 3.141592654f

#include <cuda_runtime.h>

#ifdef CPU
typedef unsigned int uint;
typedef double REAL;
#else
typedef float REAL;
#endif

typedef r123::Philox2x32 RNG;

__device__
float2 uniform(int seed, int nx, int ny);

__device__
float2 box_muller(int seed, int nx, int ny);


__device__
uint cell_index(uint i, uint central, float3 L, float2 cell, int *translate);

__device__
REAL yukawa(REAL r, REAL cutoff);

__host__ __device__
REAL i1(REAL x);

__host__ __device__
REAL k1(REAL x, REAL cutoff);

