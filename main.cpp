#include <vector>
#include <iostream>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <getopt.h>
#include <cstdlib>
#include <cstdio>
#include <algorithm>

#include "vortex.h"
#include "my_h5.h"

#define PRECI .0000001
//#define MINFTY

using namespace std;


template <typename T>
struct a_equal
{
    T val;

    a_equal(T val ) : val(val) {}

    bool operator()(T a){
        return abs(a-val) < PRECI;
    }
};

void progressbar(int progress, int max_val){

    progress = 100*progress/max_val;

    if(progress> 100) return;

    std::cout<<"[";
    for(int i=0; i != 100; ++i)
        if(i < progress)
            std::cout << '=';
        else if( i == progress)
            std::cout << '>';
        else
            std::cout << ' ';
    std::cout<< "] " << progress << " %" << '\r';
    std::cout.flush();
}


template <typename T>
void generate_rampa_vals(vector<T> &v, T up, T down, int n_point)
{
    T dT = (down-up)/static_cast<T>(n_point);

    for (int i = 0; i < n_point+1; ++i){
        v.push_back(up+i*dT);
    }
    dT *= -1;
    for (int i = 1; i < n_point+1; ++i){
        v.push_back(down+i*dT);
    }
    dT *= -1;
    for (int i = 1; i < n_point+1; ++i){
        v.push_back(up+i*dT);
    }
    /*//Esto es nuevo/////
    for (int i = 1; i < n_point+1; ++i){
        v.push_back(up+n_point*dT+i*dT);
    }
    dT *= -1;
    for (int i = 1; i < 2*n_point+1; ++i){
        v.push_back(2*down+i*dT);
    }
    dT *= -1;
    for (int i = 1; i < 2*n_point+1; ++i){
        v.push_back(up+i*dT);
    }
*/
}

template <typename T>
void slice_vals(const vector<T> & in, vector<T> & out, Parms p, int last_step, int delta=0){
    int term, mes;
    term = p.term_steps;
    mes = p.mes_steps;
    auto it = find_if(in.begin(), in.end(), a_equal<T>(p.drive_x));

    if (it != in.end()){
        int index = int(it-in.begin());
        cout << "firsh index finded : " << index << endl;
        cout << "steps : " << (index + 1)*(mes+term) << endl;
        cout << "total_steps : " << last_step << endl;

        if ((index+1)*(term+mes)+delta != last_step){
            do{
                it = find_if(it+1, in.end(), a_equal<T>(p.drive_x));
                index = int(it-in.begin());

                cout << "second index finded : " << index << endl;
                cout << "steps : " << (index + 1)*(mes+term)+delta << endl;
                cout << "total_steps : " << last_step << endl;

                /*if ((index+1)*(term+mes) != last_step){
                it = find_if(it+1, in.end(), a_equal<T>(p.drive_x));
                index = int(it-in.begin());
                cout << "second index finded : " << index << endl;
                cout << "steps : " << (index + 1)*(mes+term) << endl;
                cout << "total_steps : " << last_step << endl;
            }*/
                if (it == in.end()) break;
            }while((index+1)*(term+mes)+delta != last_step);
        }

    }else{
        cout << "the element isn't in the vector" << endl;
    }

    int index = int(it-in.begin());
    if ((it != in.end())&&((index+1)*(mes+term)+delta == last_step)){
        out.resize(int(in.end()-it)-1);
        copy(it+1, in.end(), out.begin());
    }else{
        cout << "condition doesn't match" << endl;
    }
}

REAL term_and_messure(vortex *s, my_h5 *f, int term_steps, int MC_steps, bool messure=true)
{
    int j = 0;
    int i = 0;
    auto start = chrono::steady_clock::now();
    cout << "termalization..." << endl;
    while (i < term_steps) {
        j = 0;
        while (j < 50) {
            s->update_positions();
            ++j;
        }
        i += 50;
//        progressbar(i, term_steps);
    }
    cout << "\ndone" << endl;

    i = 0;

    cout << "messuring..." << endl;

    Parms p = s->get_parms();
    while (i < MC_steps) {
        j = 0;
        while (j < 50) {
            s->update_positions();
            //if (((j+i)%p.mes_each) == 0) {
                //s->acumulate();
            //}
            ++j;
        }
        i += 50;
//        progressbar(i, term_steps);
    }

    cout << "\ndone" << endl;

#ifndef CPU
    cudaDeviceSynchronize();
#endif
    auto end = chrono::steady_clock::now();

    REAL elapsed_time = chrono::duration_cast<chrono::duration<REAL>>(end-start).count();
    cout << "elapsed time : " << elapsed_time << endl;
    if (messure){
#ifdef THRUST_DEBUG
        printf("writing in hdf5 file\n");
#endif
        s->write_hdf5(*f, elapsed_time, MC_steps, term_steps);
    }
#ifndef CPU
    cudaDeviceSynchronize();
#endif
    return elapsed_time;
}

template<typename T>
void rampa(vortex *sis, my_h5 *file, Parms *parms, const vector<T> &v, bool temp_ramp = false)
{

    for (auto it = v.begin(); it != v.end(); ++it) {
        if (temp_ramp){
            parms->T = *it;
            printf("\nTemperature : %f\n", parms->T);
        }else{
            parms->drive_x = *it;
            printf("\ndrive_x : %f\n", parms->drive_x);
        }
        sis->set_parms(*parms);
        sis->reset_counters();
        term_and_messure(sis, file, parms->term_steps, parms->mes_steps);
    }
}

int main(int argc, char **argv)
{

    Parms parms;
    if (!parms.set_by_command_line(argc, argv)){
        return 0;
    }

    my_h5 * file = 0;//(file_name);
    vortex * sis = 0; //(parms);

    int init_term = 0;
    
    if (parms.file=="none"){
        parms.file = "result_L_("+to_string(parms.Lx)+","+to_string(parms.Ly)+
            ")_FP_"+to_string(parms.pin_force)+
            "_a0_"+to_string(parms.a0)+
            "_T_"+to_string(parms.T)+
            "_M_"+
#ifndef MINFTY
            to_string(parms.MM)+
#else
            "INF"+
#endif
            ".h5";
      file = new my_h5(parms.file);
      sis = new vortex(parms);
      init_term = 80;
    }else{
       file = new my_h5(parms.file);
       parms.load_from_file(*file);
       sis = new vortex(parms);
       sis->load_state(*file);
    }
    
    std::cout << parms.file << std::endl;
//    parms.term_steps = 5000;
//    parms.mes_steps = 5000;
   REAL init_T = parms.T;
   sis->set_parms(parms);
   
//   if (init_term){
//       parms.T = 0;
//       sis->set_parms(parms);
//       term_and_messure(sis, file, init_term*parms.term_steps/4, 0, false);
//       parms.T = .25*init_T;
//       sis->set_parms(parms);
//       term_and_messure(sis, file, init_term*parms.term_steps/4, 0, false);
//       parms.T = .5*init_T;
//       sis->set_parms(parms);
//       term_and_messure(sis, file, init_term*parms.term_steps/4, 0, false);
//       parms.T = init_T;
//       sis->set_parms(parms);
    term_and_messure(sis, file, 20*parms.term_steps, 0, false);
//   }
   for(int n=0; n < 40; n++){
       term_and_messure(sis, file, parms.term_steps, parms.mes_steps);
   }

    return 0;
}
