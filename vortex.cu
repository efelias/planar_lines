#include "vortex.h"

struct poisson_generator
{
    int pins_per_cell, seed;
    REAL L;
    poisson_generator(int NPC, int seed) :
        pins_per_cell(NPC), seed(seed),
        L(expf(-REAL(NPC)))
    {}
    __device__
    int operator()(int i){
        REAL p = 1.0;
        int k = 0;
        while (p > L) {
            k++;
            p = p*uniform(seed, i, k).x;
        }
        return k-1;
    }
};

struct update_u0
{
    REAL *x, *y, *ux, *uy;
    float2 CM;
    update_u0(REAL *x, REAL *y, REAL *ux, REAL *uy, float2 CM) :
        x(x), y(y), ux(ux), uy(uy), CM(CM) {}
    __device__
    void operator()(int i){
        ux[i] = (x[i]-CM.x);
        uy[i] = (y[i]-CM.y);
    }
};


struct square_diff
{
    REAL *x, *y, *ux0, *uy0;
    float2 CM;
    square_diff(REAL *x, REAL *y, REAL *ux0, REAL *uy0, float2 CM) :
        x(x), y(y), ux0(ux0), uy0(uy0), CM(CM) {}
    __device__
    vec2 operator()(int i){
        REAL ux = (x[i]-CM.x-ux0[i]);
        REAL uy = (y[i]-CM.y-uy0[i]);
        return vec2(ux*ux, uy*uy);
    }
};

struct add_vec2
{
    __device__
    vec2 operator()(vec2 a, vec2 b){
        REAL a1 = thrust::get<0>(a)+thrust::get<0>(b);
        REAL a2 = thrust::get<1>(a)+thrust::get<1>(b);
        return vec2(a1, a2);
    }
};

struct make_random_vec2
{
    int seed;
    REAL Lx, Ly;
    make_random_vec2(int seed, REAL Lx, REAL Ly) :
        seed(seed), Lx(Lx), Ly(Ly){}
    __device__
    thrust::tuple<REAL, REAL> operator()(int n)
    {
        float2 r = uniform(seed, n, 0);
        return thrust::make_tuple(Lx*r.x, Ly*r.y);
    }
};

struct points_to_bucket
{
    float3 L;
    float2 cell;
    points_to_bucket(float3 L, float2 cell) :
        L(L), cell(cell) {}
    __device__
    uint operator()(thrust::tuple<REAL, REAL> i){
        REAL xx = thrust::get<0>(i);
        REAL yy = thrust::get<1>(i);
        xx -= floor(xx/L.x)*L.x;
        yy -= floor(yy/L.y)*L.y;
        //assert((xx > 0)&&(xx <= L.x));
        uint x = static_cast<uint>(xx/cell.x);
        uint y = static_cast<uint>(yy/cell.y);
        return x + uint(L.x/cell.x)*y;
    }
};

vortex::vortex(Parms parms, init_mode mode) :
    parms(parms), step(0), messure_counter(0)
{
    parms.print();

    uint nx = floor(parms.L.x/parms.cutoff);
    uint ny = floor(parms.L.y/parms.cutoff);
    N_cells = nx*ny;
    printf("number of cells : %i\n", N_cells);
    cell.x = parms.L.x/nx;
    cell.y = parms.L.y/ny;

    particles_per_cell = parms.N/N_cells;
    if (parms.mes_steps <= 2000){
        n_difussion_acumulate = 2;
    }else{
        n_difussion_acumulate = 4;
    }
    _init(mode);
}

void vortex::update_positions()
{
    Force force_computator(x, z, parms.L, parms.cutoff, parms.Lx, parms.Ly, parms.MM);

    thrust::transform(
                thrust::make_counting_iterator(0),
                thrust::make_counting_iterator(parms.N),
                thrust::make_zip_iterator(
                    thrust::make_tuple(fx_d.begin(), fy_d.begin())),
                      force_computator
                );

    euler eu(parms.dt, step++, sqrt(2.0*abs(parms.T)),
             parms.L.x, parms.L.y, parms.drive_x);

    int N = parms.N;

    auto beg = thrust::make_zip_iterator(
                thrust::make_tuple(
                    thrust::make_counting_iterator(0),
                    x_d.begin(),fx_d.begin(),
                    z_d.begin(), fy_d.begin()
                    )
                );

    auto end = thrust::make_zip_iterator(
                thrust::make_tuple(
                    thrust::make_counting_iterator(int(N)),
                    x_d.end(), fx_d.end(),
                    z_d.end(), fy_d.end()
                    )
                );

    auto point_beg = thrust::make_zip_iterator(
                thrust::make_tuple(
                    x_d.begin(), z_d.begin()
                    )
                );

    thrust::transform(
                beg, end, point_beg, eu
                );
}


void vortex::set_parms(Parms _parms)
{
    parms = _parms;
}

void vortex::acumulate()
{
    uint max_counter = parms.mes_steps/n_difussion_acumulate/parms.mes_each;

    if ((messure_counter == 0)||(messure_counter%max_counter == 0))
        update_initial_u();

    vec2 U2 = thrust::transform_reduce(thrust::make_counting_iterator(0),
                                       thrust::make_counting_iterator(parms.N),
                                       square_diff(x, y, ux0, uy0, get_CM()),
                                       vec2(0.0, 0.0), add_vec2());

    vec2 V = thrust::reduce(thrust::make_zip_iterator(
                                thrust::make_tuple(vx_d.begin(), vy_d.begin())
                                ),
                            thrust::make_zip_iterator(
                                thrust::make_tuple(vx_d.end(), vy_d.end())
                                ),
                            vec2(0.0,0.0), add_vec2());

    mean_vx.push_back(thrust::get<0>(V)/parms.N);
    mean_vy.push_back(thrust::get<1>(V)/parms.N);

    if (messure_counter < max_counter){
        mean_ux2.push_back(thrust::get<0>(U2)/parms.N);
        mean_uy2.push_back(thrust::get<1>(U2)/parms.N);
    }else{
        uint index = messure_counter%max_counter;
        uint s = messure_counter/max_counter;

        mean_ux2[index] = mean_ux2[index]+(thrust::get<0>(U2)/parms.N-mean_ux2[index])/(s+1);
        mean_uy2[index] = mean_uy2[index]+(thrust::get<1>(U2)/parms.N-mean_uy2[index])/(s+1);
    }

    messure_counter++;
}

void vortex::reset_counters()
{
    messure_counter = 0;
    mean_vx.resize(0);
    mean_vy.resize(0);
    mean_ux2.resize(0);
    mean_uy2.resize(0);
    thrust::fill(dens_acum_d.begin(), dens_acum_d.end(), 0);
}

float2 vortex::get_CM()
{
    REAL Cx = thrust::reduce(x_d.begin(), x_d.end(), REAL(0), thrust::plus<REAL>())/parms.N;
    REAL Cy = thrust::reduce(y_d.begin(), y_d.end(), REAL(0), thrust::plus<REAL>())/parms.N;
    return make_float2(Cx, Cy);
}

void vortex::_init(init_mode mode)
{
    printf("mean number of particles per cell : %i\n", particles_per_cell);

    size_t N = size_t(parms.N);

    x_d.resize(N);
    y_d.resize(N);
    vx_d.resize(N);
    vy_d.resize(N);
    ux0_d.resize(N);
    uy0_d.resize(N);
    z_d.resize(N);
    fx_d.resize(N);
    fy_d.resize(N);
    N_pins_per_cell_d.resize(N_cells);

    nx = int(parms.L.x/(parms.a0/3));
    ny = int(parms.L.y/(parms.a0/3));

    dens_acum_d.resize(nx*ny);


    thrust::fill(z_d.begin(), z_d.end(), 0);
    thrust::fill(fx_d.begin(), fx_d.end(), 0);
    thrust::fill(fy_d.begin(), fy_d.end(), 0);
    thrust::fill(dens_acum_d.begin(), dens_acum_d.end(), 0);

    x = thrust::raw_pointer_cast(&x_d[0]);
    y = thrust::raw_pointer_cast(&y_d[0]);
    ux0 = thrust::raw_pointer_cast(&ux0_d[0]);
    uy0 = thrust::raw_pointer_cast(&uy0_d[0]);
    vx = thrust::raw_pointer_cast(&vx_d[0]);
    vy = thrust::raw_pointer_cast(&vy_d[0]);
    z = thrust::raw_pointer_cast(&z_d[0]);

    fx = thrust::raw_pointer_cast(&fx_d[0]);
    fy = thrust::raw_pointer_cast(&fy_d[0]);
    N_pins_per_cell = thrust::raw_pointer_cast(&N_pins_per_cell_d[0]);


    thrust::transform(thrust::make_counting_iterator(0),
                      thrust::make_counting_iterator(int(N_cells)),
                      N_pins_per_cell_d.begin(),
                      poisson_generator(parms.pins_per_cell, parms.N_pins_seed));

    _init_positions(mode);

    printf("system inicilized\n");
}

void vortex::_init_positions(init_mode mode)
{
    int N = parms.N;
    REAL Lx = parms.L.x;
    REAL Ly = parms.L.y;
    REAL w = 2.0;//parms.a0*sqrtf(1.-0.5*parms.MM*parms.a0/k1(parms.a0, 5));
    int seed = time(nullptr);

    switch (mode) {
    case RANDOM:
        thrust::transform(thrust::make_counting_iterator(0),
                          thrust::make_counting_iterator(int(N)),
			  thrust::make_zip_iterator(thrust::make_tuple(x_d.begin(), y_d.begin())),
                          make_random_vec2(seed, Lx, Ly));
        break;

    case REGULAR:
        thrust::transform(thrust::make_counting_iterator(0),
                          thrust::make_counting_iterator(int(N)),
			  thrust::make_zip_iterator(thrust::make_tuple(x_d.begin(), y_d.begin())),
                          regular_grid(Lx, Ly, parms.a0));
        break;
    case HEXAGONAL:
        thrust::transform(thrust::make_counting_iterator(0),
                          thrust::make_counting_iterator(int(N)),
			  thrust::make_zip_iterator(thrust::make_tuple(x_d.begin(), y_d.begin())),
                          hexagonal_grid(Lx, Ly, parms.a0));
        break;
    case ZIGZAG:
	printf("zigzag position\n");
	thrust::transform(thrust::make_counting_iterator(0),
			  thrust::make_counting_iterator(int(N)),
			  thrust::make_zip_iterator(thrust::make_tuple(x_d.begin(), y_d.begin(), z_d.begin())),
			  zig_zag(Lx, Ly, parms.a0, w));
	break;
    }
}

void vortex::update_initial_u()
{
#ifdef THRUST_DEBUG
    printf("updating U0...\n");
#endif
    float2 CM = get_CM();
    thrust::for_each(thrust::make_counting_iterator(0),
                     thrust::make_counting_iterator(parms.N),
                     update_u0(x, y, ux0, uy0, CM));
}

void vortex::write_hdf5(my_h5 & file, REAL elapsed_time, int steps, int term_steps)
{
    //atributes
#ifndef CPU
    cudaDeviceProp deviceProp;
    cudaGetDeviceProperties(&deviceProp, 0);
    string name = deviceProp.name;
    cout << name << endl;
    file.set_atribute("running_time", elapsed_time, true);
    file.set_atribute("video_card", name);
#else
    file.set_atribute("running_time", elapsed_time, true);
#endif
    file.set_atribute("steps", steps);
    file.set_atribute("term_steps", term_steps);
    file.set_atribute("Last_step", int(step));
    file.set_atribute("Lx", parms.L.x);
    file.set_atribute("Ly", parms.L.y);
    file.set_atribute("Lz", parms.L.z);
    file.set_atribute("a0", parms.a0);
    file.set_atribute("N", parms.N);
    file.set_atribute("cutoff", parms.cutoff);
    file.set_atribute("drive_x", parms.drive_x);
    file.set_atribute("drive_y", parms.drive_y);
    file.set_atribute("magnus_ratio", parms.M);
    file.set_atribute("dt", parms.dt);
    file.set_atribute("T", parms.T);
    file.set_atribute("pins_per_cell", parms.pins_per_cell);
    file.set_atribute("pin_force", parms.pin_force);
    file.set_atribute("pin_cutoff", parms.pin_cutoff);
    file.set_atribute("N_pins_seed", parms.N_pins_seed);
    file.set_atribute("pos_pin_seed", parms.pos_pin_seed);
    file.set_atribute("MM", parms.MM);

#ifdef THRUST_DEBUG
    printf("saving data...\n");
    printf("allocating N vector\n");
#endif

    thrust::host_vector<REAL> temp(size_t(parms.N));

#ifdef THRUST_DEBUG
    printf("saving x pos...\n");
#endif
    temp = x_d;
    file.add_data("X", temp);

#ifdef THRUST_DEBUG
    printf("saving y pos...\n");
#endif
    temp = y_d;
    file.add_data("Y", temp);

#ifdef THRUST_DEBUG
    printf("saving z pos...\n");
#endif
    temp = z_d;
    file.add_data("Z", temp);

    file.close_file();
}

void vortex::load_state(my_h5 &file, int index)
{
    //std::queue<std::string> groups = file.print_objets();
    printf("get last group\n");
    //std::string last_group = groups.back();
    std::string last_group = file.get_last_obj(index);

    thrust::host_vector<REAL> temp_vect(parms.N);
    printf("reading X\n");
    file.read_data(last_group, "X", temp_vect);
    thrust::copy(temp_vect.begin(), temp_vect.end(), x_d.begin());

    printf("reading Y\n");
    file.read_data(last_group, "Y", temp_vect);
    thrust::copy(temp_vect.begin(), temp_vect.end(), y_d.begin());

    printf("reading Z\n");
    file.read_data(last_group, "Z", temp_vect);
    thrust::copy(temp_vect.begin(), temp_vect.end(), z_d.begin());

    int temp;
    file.read_attribute(last_group, "Last_step", temp);
    printf("reading las steps, value %i\n", temp);
    step = uint(temp);


}

void vortex::check_dt()
{
    REAL max_vx = thrust::reduce(vx_d.begin(), vx_d.end(), REAL(0), thrust::maximum<REAL>());
    REAL max_vy = thrust::reduce(vy_d.begin(), vy_d.end(), REAL(0), thrust::maximum<REAL>());
    REAL M = std::max(max_vx, max_vy)*parms.dt;
    if ((M > parms.pin_cutoff*.1) && (parms.dt > .001)){
        parms.dt *= .95;
        std::cout << "reducing dt... dt = " << parms.dt << "\n";
    }else if ((M < parms.pin_cutoff*.005)&&(parms.dt < 1.0)) {
        parms.dt *= 1.05;
        std::cout << "encreasing dt... dt = " << parms.dt << "\n";
    }
}
