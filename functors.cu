#include "functors.h"
#include "Random123/philox.h"
#include "Random123/u01.h"


#define PI 3.141592654f
#define  CONS 0.26666666666666666667


//__device__
//template <typename T> int sgn(T val) {
//    return (T(0) < val) - (val < T(0));
//}

__device__
int sgn(REAL val) {
  return (REAL(0) < val) - (val < REAL(0));
}
__device__
thrust::tuple<REAL,  REAL>
euler::operator()(thrust::tuple<int, REAL,  REAL, REAL, REAL> data)
{
  float2 Nrn = box_muller(0, time, thrust::get<0>(data));

  REAL fx = thrust::get<2>(data);
  REAL fz = thrust::get<4>(data);
  REAL xx = thrust::get<1>(data);
  REAL zz = thrust::get<3>(data);

  fx += Nrn.x*termal_factor;
  fz += Nrn.y*termal_factor;
  //printf("%f, %f\n", Nrn.x, Nrn.y);
  REAL x_new = xx + fx*dt;
  REAL z_new = zz;
#ifndef MINFTY
  z_new += fz*dt;
#endif
  return thrust::tuple<REAL,  REAL>(x_new, z_new);
}

__device__
vec2 regular_grid::operator()(int i){
  int x = i%int(Lx/a0);
  int y = i/int(Lx/a0);
  return vec2(a0*x+.1, y+.1);
}

__device__
vec3 zig_zag::operator()(int i){
  int x = i%int(Lx/a0);
  int y = i/int(Lx/a0);
  REAL z = 0.5*w*(2*(x%2)-1);
  //     printf(" z %i = %f\n", x, z);
  return vec3(a0*x+.1, y+.1, z);
}

__device__
vec2 hexagonal_grid::operator()(int i){
  int x = i%int(Lx/a0);
  int y = i/int(Lx/a0);
  return vec2(a0*(x + 0.5*(y%2)+.1), a0*(y*sqrt(3.0)/2.0)+.1);
}

__device__
vec2 Force::operator()(int i)
{
  REAL f = REAL(0.0);
  REAL fz = REAL(0.0);
  REAL F1, F2, d1, d2;
  int nx = i%lx;
  int ny = i/lx;
  REAL xx, xp, xm, dxm, dxp;
  REAL zz, zp, zm, dzp, dzm;
  xx = x[i];//centerx
  zz = z[i];//centerx

  xp = x[nx+((ny+1)%ly)*lx];//neig_1+_layer
  xm = x[nx+((ny-1+ly)%ly)*lx];//neig_1-_layer

  zp = z[nx+((ny+1)%ly)*lx];//neig_1+_layer
  zm = z[nx+((ny-1+ly)%ly)*lx];//neig_1-_layer


  f+= xp+xm-2*xx;//elastic_force
  fz+= zp+zm-2*zz;//elastic_force


  int n_neight = 4;

  for (int n = 1; n < n_neight+1; n++)
    {
      xp = x[(nx+n)%lx+ny*lx];//neig_n+
      xm = x[(nx-n+lx)%lx+ny*lx];//neig_n-

      zp = z[(nx+n)%lx+ny*lx];//neig_n+
      zm = z[(nx-n+lx)%lx+ny*lx];//neig_n-

      dxp = xp-xx;
      dxp -= L.x*round(dxp/L.x);
      dxm = xm-xx;
      dxm -= L.x*round(dxm/L.x);

      dzp = zp-zz;
      dzm = zm-zz;

      d1 = std::sqrt(dxp*dxp+dzp*dzp);
      d2 = std::sqrt(dxm*dxm+dzm*dzm);

      F1 = k1(d1, cutoff);
      F2 = k1(d2, cutoff);

      f+= -(F1*dxp/d1+F2*dxm/d2);
      fz+= -(F1*dzp/d1+F2*dzm/d2);
    }
  fz+= -MM*zz;

  return vec2(f, fz);
}

__device__
void pin_Force::operator()(int i)
{
  uint central = cells_index[i];

  REAL Fx = 0, Fy = 0;
  uint neig_cell;
  int pins;
  REAL cx, cy, dx, dy, dr2, vpin;
  int w = int(L.x/cell.x);

  REAL xpin, ypin;

  REAL xx = x[i];
  REAL yy = y[i];
  xx -= floor(xx/L.x)*L.x;
  yy -= floor(yy/L.y)*L.y;

  int *tr = new int[2];

  for (uint n = 0; n < 9; ++n) {
      neig_cell = cell_index(n, central, L, cell, tr);
      pins = N_pins_per_cell[neig_cell];
      cx = REAL(neig_cell%w)*cell.x;
      cy = REAL(neig_cell/w)*cell.y;
      REAL rint = pin_cutoff/5;

      for (int p = 0; p < pins; ++p) {
          float2 rn = uniform(seed, neig_cell, p);
          xpin = cx + rn.x*cell.x;
          ypin = cy + rn.y*cell.y;
          dx = (xpin+tr[0]*L.x-xx);
          dy = (ypin+tr[1]*L.y-yy);

          dr2=dx*dx+dy*dy;

          if((dr2 < pin_cutoff*pin_cutoff)){
              // a gaussian attractive potential
              vpin = -pin_streng*expf(-dr2/(rint*rint)/2.0)/(rint*rint);
              //f.x+=vpin*dx; // /(rint*rint);
              //f.y+=vpin*dy; // /(rint*rint);
              Fx += -vpin*dx; // /(rint*rint);
              Fy += -vpin*dy; // /(rint*rint);
            }
        }
    }
  delete [] tr;
#ifdef THRUST_DEBUG
  if ((Fx != Fx) || (Fy != Fy)){
      printf("pin force functor: bad force value in sector %d\n", i);
      printf("pinFx : %f, pinFy : %f, Fx : %f, Fy : %f\n", Fx, Fy, fx[i], fy[i]);
      //throw;
    }
#endif
  fx[i] += Fx;
  fy[i] += Fy;
}

__device__
void acumulate_density::operator()(int n)
{
  REAL xx = x[n];
  REAL yy = y[n];
  xx -= floor(xx/L.x)*L.x;
  yy -= floor(yy/L.y)*L.y;

  int index = int(xx/gx)+nx*int(yy/gy);
  dens[index] += 1.0;
}
