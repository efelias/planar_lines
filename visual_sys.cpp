#include "visual_sys.h"
#include "Random123/philox.h"
#include "Random123/u01.h"


visual_sys::visual_sys(Parms init_parms, init_mode mode) :
  posicioner (init_parms.N), vortex (init_parms, mode)
{}

struct posicioner_functor
{
    float *x;
    float *y;
    float *z;
    float Lx, Ly;
    float *color;
    float *pos;
    //uint *cells_index;
    
    posicioner_functor(float *x, float *y, float *z, float Lx, float Ly,
                       float *color, float *pos) :
        x(x), y(y), z(z), Lx(Lx), Ly(Ly), color(color), pos(pos){}
    __device__
    void operator()(int i){
        float xx = x[i];
        float yy = y[i];
        xx -= floor(xx/Lx)*Lx;
        yy -= floor(yy/Ly)*Ly;

        pos[4*i] = yy-Ly/2;
        pos[4*i+1] = z[i];
        //if (i%100 == 0) printf("z = %f\n",z[i]);
        pos[4*i+2] = xx-Lx/2;
        pos[4*i+3] = 1.0;

        color[4*i] = 1.0;//uniform(cells_index[i], 0, 1).y;
        color[4*i+1] = 1.0;//uniform(cells_index[i], 0, 2).x;
        color[4*i+2] = 1.0;//uniform(cells_index[i], 0, 2).y;
        color[4*i+3] = 1.0;
    }
};

void visual_sys::update_positions_and_colors()
{
#ifdef CPU
  glBindBuffer(GL_ARRAY_BUFFER, posVbo);
  float *positions = (float*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
  //float* ptr = data;
  if (positions){
      for (uint i = 0; i < parms.N; ++i)
      {
          float xx = x[i];
          float yy = y[i];
          //xx -= floor(xx/parms.L.x)*parms.L.x;
          //yy -= floor(yy/parms.L.y)*parms.L.y;

          *positions++ = yy-parms.L.y/2;
          *positions++ = z[i];
          *positions++ = xx-parms.L.x/2;
          *positions++ = 1.0;
      }
      glUnmapBuffer(GL_ARRAY_BUFFER);
  }

  //this->triangulate();

  glBindBuffer(GL_ARRAY_BUFFER, colorVBO);
  float *colors = (float*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
  //float* ptr = data;
  float R,G,B;
  uint neig;
  if (colors){
      for (uint i = 0; i < parms.N; ++i)
      {
          /*
          *colors++ = uniform(cells_index[i], 0, 1).y;
          *colors++ = uniform(cells_index[i], 0, 2).x;
          *colors++ = uniform(cells_index[i], 0, 2).y;
          *
          //neig = triangles[i+1];

          if (neig == 6){
              R=1.0;G=1.0;B=1.0;
          }else if (neig < 6) {
              R=0.0;G=0.0;B=1.0;
          }else if (neig > 6) {
              R=1.0;G=0.0;B=0.0;
          }*/
          *colors++ = 1.0; *colors++=1.0;*colors++=1.0;
          *colors++ = 1.0;
      }
      glUnmapBuffer(GL_ARRAY_BUFFER);
  }
#else
    float *positions;
    float *colors;

    positions = (float *) mapGLBufferObject(&m_cuda_posvbo_resource);
    colors = (float *) mapGLBufferObject(&m_cuda_colorvbo_resource);


    posicioner_functor compute(x, y, z, parms.L.x, parms.L.y, colors, positions);

    thrust::for_each(
                thrust::make_counting_iterator(0),
                thrust::make_counting_iterator(parms.N),
                compute
                );

    unmapGLBufferObject(m_cuda_posvbo_resource);
    unmapGLBufferObject(m_cuda_colorvbo_resource);
#endif
}


