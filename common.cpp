#include "common.h"


__device__
float2 uniform(int seed, int nx, int ny)
{   /*
    Generador de numeros aleatorios en la memoria individual de cada Tread.
    Recive tres enteros de argumento y retorna una tupla (tipo float2 de CUDA) de dos
    nueros aleatoreo "unicos" para una dada terna de enteros.
    El resultado puede ser regenerado en cualquier momento con la misma terna de enteros.
    */

    // keys and counters
    RNG philox;
    RNG::ctr_type c={{}};
    RNG::key_type k={{}};
    RNG::ctr_type r;
    // Garantiza una secuencia random "unica" para cada thread
    k[0] = seed;
    c[1] = nx;
    c[0] = ny;
    r = philox(c, k);
    float2 num;
    num.x=u01_open_open_32_53(r[0]);
    num.y=u01_open_open_32_53(r[1]);
    return num;
}



__device__
float2 box_muller(int seed, int nx, int ny)
{
       // genera una distribucion gauseana
       float2 uniform123 = uniform(seed, nx, ny);
       REAL r = sqrt(-2.0*log(uniform123.x) );
       float2 Nr;
       REAL theta = 2.0*PI*uniform123.y;
       Nr.x = r*cos(theta);
       Nr.y = r*sin(theta);

#ifdef THRUST_DEBUG
    //if ((time % 200 == 0) && (thrust::get<0>(data) == 5119)) printf("x : %f, x_new : %f, y : %f, y_new : %f, fx : %f, fy : %f, vx : %f, vy : %f\n",
    //              xx, x_new, yy, y_new, fx, fy, vx, vy);
    if ((Nr.x != Nr.x) || (Nr.y != Nr.y)){
        printf("bad value: x = %f, y = %f, nx = %f, ny = %f", uniform123.x, uniform123.y, Nr.x, Nr.y);
    }
#endif
       return Nr;
}

__device__
uint cell_index(uint i, uint central, float3 L, float2 cell, int *translate){
  //CENTER CELL WHEN i == 4
  int nx = i%3-1;
  int ny = i/3-1;

  uint w = uint(L.x/cell.x);
  uint h = uint(L.y/cell.y);

  int x = (central%w+nx);
  int y = (central/w+ny);

  translate[0] = (x < 0)?-1:0+((x > w-1)?1:0);//no da el valor esperado
  translate[1] = (y < 0)?-1:0+((y > h-1)?1:0);

  return ((x+w)%w)+((y+h)%h)*w;
}



__device__
REAL yukawa(REAL r, REAL cutoff){
    REAL rint = cutoff/5.0;
  return -exp(-r/rint)*(1/r/rint+1/r/r);
}

__host__ __device__
REAL i1(REAL x)
{
    REAL   ax, y, ans;

    if ((ax=fabs(x)) < 3.75) {
       y = x*CONS;
       y *= y;
       ans = ax*(0.5+y*(0.87890594+y*(0.51498869+y*(0.15084934
           + y*(0.2658733e-1+y*(0.301532e-2+y*0.32411e-3))))));
    } else {
       y = 3.75/ax;
       ans = 0.2282967e-1+y*(-0.2895312e-1+y*(0.1787654e-1
           - y*0.420059e-2));
       ans = 0.39894228+y*(-0.3988024e-1+y*(-0.362018e-2
           + y*(0.163801e-2+y*(-0.1031555e-1+y*ans))));
       ans *= (exp(ax)/sqrt(ax));
    }
    return ans;
}

__host__ __device__
REAL k1(REAL x, REAL cutoff)
{   REAL rint = cutoff/5.0;
    x = x/rint;

    REAL   y, ans;

    if (x <= 2.0) {
       y = x*x*0.25;
       ans = (log(x*0.5)*i1(x))+(1.0/x)*(1.0+y*(0.15443144
           + y*(-0.67278579+y*(-0.18156897+y*(-0.1919402e-1
           + y*(-0.110404e-2+y*(-0.4686e-4)))))));
    } else {
       y = 2.0/x;
       ans = (exp(-x)/sqrt(x))*(1.25331414+y*(0.23498619
           + y*(-0.3655620e-1+y*(0.1504268e-1+y*(-0.780353e-2
           + y*(0.325614e-2+y*(-0.68245e-3)))))));
    }
    return ans;
}

