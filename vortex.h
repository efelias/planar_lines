#ifndef VORTEX_H
#define VORTEX_H


#include <iostream>
#include <fstream>
#include <cmath>
#include <cstdio>
#include <cstdlib>

#include <iterator>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/transform_reduce.h>
#include <thrust/reduce.h>
#include <thrust/for_each.h>
#include <thrust/copy.h>
#include <thrust/sort.h>
#include <thrust/fill.h>
#include <thrust/tuple.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/iterator/transform_iterator.h>
#include <thrust/iterator/permutation_iterator.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/functional.h>
#include <thrust/extrema.h>
#include <thrust/random.h>
#include <thrust/binary_search.h>
#include <ctime>
#include <boost/program_options.hpp>

#include "functors.h"
#include "my_h5.h"

#include <vector>
#include <thrust/adjacent_difference.h>

using namespace std;
namespace po = boost::program_options;

struct Parms
{
    float3 L;
    int Lx, Ly;
    int N;
    REAL a0;
    REAL cutoff;
    REAL drive_x;
    REAL drive_y;
    REAL M;
    REAL dt;
    REAL T;
    int pins_per_cell; //mean number of pins per cell
    REAL pin_force;   //amplitude of the gaussian pin potential
    REAL pin_cutoff;  //interaction range seted to pin_cutoff/5
    int N_pins_seed;
    int pos_pin_seed;
    int term_steps;
    int mes_steps;
    int mes_each;
    REAL MM;
    string file;

    Parms(){}

    Parms(int _Lx, int _Ly, REAL _a0) :
        L(make_float3(_a0*_Lx, _a0*_Ly, 1.0)), Lx(_Lx), Ly(_Ly),
        N(_Lx*_Ly), a0(_a0), cutoff(5), drive_x(0.0f),
        drive_y(0.0f), M(0.0f), dt(.05f), T(.000f),
        pins_per_cell(4), pin_force(.51),
        pin_cutoff(2), N_pins_seed(1234),
        pos_pin_seed(12345), mes_each(50), MM(1.0),
        file("none")
    {}
    void print(){
        cout << " Lx           : " << L.x           << endl;
        cout << " Ly           : " << L.y           << endl;
        cout << " Lz           : " << L.z           << endl;
        cout << " a0           : " << a0            << endl;
        cout << " N            : " << N             << endl;
        cout << " cutoff       : " << cutoff        << endl;
        cout << " drive_x      : " << drive_x       << endl;
        cout << " drive_y      : " << drive_y       << endl;
        //cout << " Magnus ratio : " << M             << endl;
        cout << " dt           : " << dt            << endl;
        cout << " T            : " << T             << endl;
        cout << " pins         : " << pins_per_cell << endl;
        cout << " pin force    : " << pin_force     << endl;
        cout << " pin cutoff   : " << pin_cutoff    << endl;
        cout << " N pins seed  : " << N_pins_seed   << endl;
        cout << " pos pin seed : " << pos_pin_seed  << endl;
        cout << " term steps   : " << term_steps    << endl;
        cout << " mes steps    : " << mes_steps     << endl;
        cout << " mes each     : " << mes_each      << endl;
        cout << " parabola mass: " << MM            << endl;
        cout << " init file    : " << file          << endl;
    }

    int set_by_command_line(int ac, char** av){
        po::options_description desc("2D MD program with disorder");
        desc.add_options()
                ("help,h", "show usage")
                ("linear_size,L", po::value<int>(&Lx)->default_value(36), "linear size in the x direction")
                ("layers,l", po::value<int>(&Ly)->default_value(36), "linear size in the y direction")
                ("a0,a", po::value<REAL>(&a0)->default_value(2.0), "mean particle distance")
                ("cutoff, c", po::value<REAL>(&cutoff)->default_value(5.0), "cutoff distance of the interparticle interaction")
                ("drive_x,x", po::value<REAL>(&drive_x)->default_value(0.0), "drive force in the x direction")
                ("drive_y,y", po::value<REAL>(&drive_y)->default_value(0.0), "drive force in the y direction")
                //("magnus_ratio,M", po::value<REAL>(&M)->default_value(0.0), "magnus force inensity ratio")
                ("dt,t", po::value<REAL>(&dt)->default_value(0.05), "integration step")
                ("temperature,T", po::value<REAL>(&T)->default_value(0.0), "temperature")
                ("pins,p", po::value<int>(&pins_per_cell)->default_value(4), "mean number of pins per cell")
                ("pin_force", po::value<REAL>(&pin_force)->default_value(.5), "pin force")
                ("pin_cutoff", po::value<REAL>(&pin_cutoff)->default_value(2.0), "interaction range of the pinning centers")
                ("pin_seed_n", po::value<int>(&N_pins_seed)->default_value(1234), "seed for generation the number of pin centers per cell")
                ("pin_seed_p", po::value<int>(&pos_pin_seed)->default_value(12345), "seed for generation the positions of pin centers")
                ("term_steps", po::value<int>(&term_steps)->default_value(0), "equilibration steps")
                ("mes_steps", po::value<int>(&mes_steps)->default_value(100), "messure steps")
                ("mes_each", po::value<int>(&mes_each)->default_value(50), "messure each N steps")
                ("MM, M", po::value<REAL>(&MM)->default_value(1.0), "parabola mass")
                ("file,F", po::value<string>(&file)->default_value("none"), "init file")
                ;

        po::variables_map vm;
        po::store(po::parse_command_line(ac, av, desc), vm);
        po::notify(vm);

        if (vm.count("help")) {
            cout << desc << "\n";
            return 0;
        }
        L = make_float3(a0*Lx, Ly, 1);
        N = Lx*Ly;
        return 1;
    }

    void load_from_file(my_h5 &file, int index=-1){
        //std::queue<std::string> groups = file.print_objets();
        //std::string last_group = groups.back();
        std::string last_group = file.get_last_obj(index);
        file.read_attribute(last_group, "N", N);
        file.read_attribute(last_group, "a0", a0);
        file.read_attribute(last_group, "Lx", Lx);
        file.read_attribute(last_group, "Ly", Ly);
        Lx/=a0;
        Ly;
        L = make_float3(a0*Lx, Ly, 1);
        file.read_attribute(last_group, "cutoff", cutoff);
        file.read_attribute(last_group, "drive_x", drive_x);
        file.read_attribute(last_group, "drive_y", drive_y);
        file.read_attribute(last_group, "dt", dt);
        file.read_attribute(last_group, "T", T);
        file.read_attribute(last_group, "pins_per_cell", pins_per_cell);
        file.read_attribute(last_group, "pin_force", pin_force);
        file.read_attribute(last_group, "pin_cutoff", pin_cutoff);
        file.read_attribute(last_group, "N_pins_seed", N_pins_seed);
        file.read_attribute(last_group, "pos_pin_seed", pos_pin_seed);
        file.read_attribute(last_group, "steps", mes_steps);
        file.read_attribute(last_group, "term_steps", term_steps);
        file.read_attribute(last_group, "MM", MM);
        try{
            file.read_attribute(last_group, "magnus_ratio", M);
        }catch(...){
            ;
        }

    }
};

class vortex
{
protected:
    thrust::device_vector<REAL> x_d;
    thrust::device_vector<REAL> y_d;
    thrust::device_vector<REAL> z_d;


    thrust::device_vector<REAL> ux0_d;
    thrust::device_vector<REAL> uy0_d;

    thrust::device_vector<REAL> vx_d;
    thrust::device_vector<REAL> vy_d;

    thrust::host_vector<REAL> mean_vx;
    thrust::host_vector<REAL> mean_vy;

    thrust::host_vector<REAL> mean_ux2;
    thrust::host_vector<REAL> mean_uy2;

    thrust::device_vector<REAL> fx_d;
    thrust::device_vector<REAL> fy_d;

    thrust::device_vector<int> N_pins_per_cell_d;
    thrust::device_vector<REAL> dens_acum_d;

    Parms parms;

    uint N_cells, particles_per_cell, step, messure_counter;
    
    float2 cell;

    int nx, ny;

    REAL termal_factor;

    REAL *x;
    REAL *y;

    REAL *ux0;
    REAL *uy0;

    REAL *vx;
    REAL *vy;
    REAL *z;

    REAL *fx;
    REAL *fy;

    int *N_pins_per_cell;

    uint n_difussion_acumulate;

public:
    enum init_mode
    {
        RANDOM,
        REGULAR,
        HEXAGONAL,
        ZIGZAG
    };

    vortex(Parms parms, init_mode mode = init_mode::ZIGZAG);

    void update_positions();
    void set_parms(Parms);
    void acumulate();
    void acumulate_sq();
    void reset_counters();
    float2 get_CM();
    uint get_step(){return step;}
    Parms get_parms(){return parms;}
    void print_parms()
    {
        parms.print();
    }

    void write_hdf5(my_h5 &file, REAL elapsed_time, int steps, int term_steps);
    void write_instant_sq(my_h5 &file);
    void load_state(my_h5 &file, int index=-1);
    void check_dt();
//#ifdef CPU
    //void triangulate();
    //REAL get_defect_density();
//#endif
protected:
    void _init(init_mode mode);
    void _init_positions(init_mode mode);
    void update_initial_u();
};

#endif // VORTEX_H
