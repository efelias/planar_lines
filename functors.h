#ifndef FUNCTORS_H
#define FUNCTORS_H
#include <thrust/tuple.h>
#include <cmath>

#include <cuda_runtime.h>
#include "common.h"


//#define LASER
#define LASER_RADIUS 20.00

#ifdef CPU
typedef unsigned int uint;
typedef double REAL;
#else
#include <cufft.h>
typedef float REAL;
#endif

typedef thrust::tuple<REAL, REAL> vec2;
typedef thrust::tuple<REAL, REAL, REAL> vec3;


struct euler
{
    REAL dt;
    int time;
    REAL termal_factor;
    REAL Lx, Ly;
    REAL drive_x;

    euler(REAL dt, int time, REAL termal_factor,
          REAL Lx, REAL Ly, REAL drive_x) :
        dt(dt), time(time),
        termal_factor(termal_factor),
        Lx(Lx), Ly(Ly), drive_x(drive_x)
    {
        termal_factor = termal_factor*sqrtf(2.0/dt);
    }
    __device__
    thrust::tuple<REAL,  REAL>
    operator()(thrust::tuple<int, REAL,  REAL, REAL, REAL> data);
};

struct acumulate_density
{
    REAL *dens, *x, *y;
    int nx, ny;
    float3 L;
    REAL gx, gy;

    acumulate_density(REAL *dens, REAL *x, REAL *y, int nx, int ny, float3 L) :
        dens(dens), x(x), y(y), nx(nx), ny(ny), L(L){
        gx = L.x/nx;
        gy = L.y/ny;
    }
    __device__
    void operator()(int);
};

struct regular_grid
{
    REAL Lx, Ly, a0;

    regular_grid(REAL Lx, REAL Ly, REAL a0) :
        Lx(Lx), Ly(Ly), a0(a0){}
    __device__
    vec2 operator()(int i);
};

struct zig_zag
{
    REAL Lx, Ly, a0, w;

    zig_zag(REAL Lx, REAL Ly, REAL a0, REAL w) :
        Lx(Lx), Ly(Ly), a0(a0), w(w){}
    __device__
    vec3 operator()(int i);
};

struct hexagonal_grid
{
    REAL Lx, Ly, a0;

    hexagonal_grid(REAL Lx, REAL Ly, REAL a0) :
        Lx(Lx), Ly(Ly), a0(a0){}
    __device__
    vec2 operator()(int i);
};

struct Force
{
  REAL *x, *z;
  float3 L;
  REAL cutoff, MM;
  int lx, ly;
  
  Force(REAL *x, REAL *z, float3 L, REAL cutoff, int lx, int ly, REAL MM) :
    x(x), z(z), L(L), cutoff(cutoff), lx(lx), ly(ly), MM(MM)
  {}
  __device__
  vec2 operator()(int);
};

struct pin_Force
{
  REAL *x, *y, *fx, *fy;
  uint *cells_index;
  int *N_pins_per_cell;
  float3 L;
  float2 cell;
  REAL pin_cutoff, pin_streng;
  int seed;

  pin_Force(REAL *x, REAL *y, REAL *fx, REAL *fy,
            uint *cells_index, int *NPC, float3 L, float2 cell,
            REAL pin_cutoff, int seed, REAL pin_streng = 1.0f) :
    x(x), y(y), fx(fx), fy(fy), cells_index(cells_index),
    N_pins_per_cell(NPC), L(L), cell(cell),
    pin_cutoff(pin_cutoff), pin_streng(pin_streng),
    seed(seed){}

  __device__
  void operator()(int);
};

#endif // FUNCTORS_H
