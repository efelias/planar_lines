#!/usr/bin/env python
# coding: utf-8
import h5py as h5
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import os
import sys
#from mpl_toolkits.axes_grid.inset_locator import (inset_axes, InsetPosition,
#                                                  mark_inset)

def get_line(x, z, n, Lx, Ly):
    line = []
    for i in range(Ly):
        line.append([x[n+i*Lx], z[n+i*Lx]])
    return np.array(line)

def get_mean_sq(file):
    print(file)
    with h5.File(file, 'r') as f:
         k = list(f.keys())
         g = f[k[-1]]

         a0 = g.attrs['a0']
         N = g.attrs['N']
         Lx = int(g.attrs['Lx']/a0)
         Ly = int(g.attrs['Ly']/a0)
         m = g.attrs['dt']
         
         qx = 2*np.pi*np.arange(1, Lx//2+1)/Lx/a0
         sqx = np.zeros_like(qx)
         cont = 0
         print(len(k))
         for i in k[:]:
             print(i)
             g = f[i]
             x, z = g['X'][...], g['Z'][...]#, g['Z'][...]
             lines = np.array([get_line(x, z, i, Lx, Ly) for i in range(Lx)])
             sq = np.zeros_like(qx)
             for l in range(Ly):
                 sq += np.array([np.abs(np.sum(np.exp(1j*qxx*lines[:, l, 0])))**2/Lx for qxx in qx])

             sqx += sq/Ly
             cont+=1
         print(i)
         sqx/=cont
    return m, np.array([qx, sqx]).T

name = sys.argv[1]

size = name[name.rfind('('):name.rfind(')')+1]

MM = name[name.rfind('M_')+2:name.rfind('.h5')]

a0 = name[name.rfind('a0_')+3:name.rfind('a0_')+4]


mm, sq = get_mean_sq(name)

fig = plt.figure(figsize=(6, 3.5))
ax = fig.add_subplot(111)
#
ax.loglog(sq[:, 0], sq[:, 1], 'o', fillstyle='none', label=mm )#color=color[cont])
plt.savefig('graf_'+size+'_'+MM+'_'+a0+'.png', dpi=100, bbox_inches='tight')

np.savetxt('s_'+size+'_'+MM+'_dt_'+str(mm)+'_'+a0+'.dat', sq)

