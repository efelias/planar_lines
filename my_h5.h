#ifndef MY_H5_H
#define MY_H5_H

#include <iostream>
#include <vector>
#include <iterator>
#include <H5Cpp.h>
#include <chrono>
#include <unordered_map>
#include <ctime>
#include <thrust/host_vector.h>
#include <string.h>
#include <stdio.h>
#include <queue>

using namespace H5;
using namespace std;

string getTimeStr();

class my_h5{
    string file_name;
    string group_name;
    H5File * file;
    Group group;
    bool is_open;
    void open_file();
    void open_group();
    void new_group();
public:
    my_h5(const string & name) :
      file_name(name),
      file(nullptr),
      is_open(false)
    {}
    void set_atribute(const string & name, const string val, bool new_g = false);
    void set_atribute(const string & name, const int val, bool new_g = false);
    void set_atribute(const string & name, const float val, bool new_g = false);
    void set_atribute(const string & name, const double val, bool new_g = false);

    void add_data(const string & name, thrust::host_vector<int> &v, bool new_g = false);
    void add_data(const string & name, thrust::host_vector<float> &v, bool new_g = false);
    void add_data(const string & name, thrust::host_vector<double> &v, bool new_g = false);

    void read_data(const string & group_name,const string & name,
                   thrust::host_vector<float> &v);
    void read_data(const string & group_name,const string & name,
                   thrust::host_vector<int> &v);
    void read_data(const string & group_name,const string & name,
                   thrust::host_vector<double> &v);

    void read_attribute(const string & group_name, const string & name, int & val);
    void read_attribute(const string & group_name, const string & name, float & val);
    void read_attribute(const string & group_name, const string & name, double & val);

    queue<string> print_objets();
    std::string get_last_obj(int index=-1);
    void read_group(const string & name);

    void close_file();
    ~my_h5();
};

#endif // MY_H5_H
