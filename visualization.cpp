// OpenGL Graphics includes
#include <GL/glew.h>
#include <GL/gl.h>

#include <GL/freeglut.h>

// Includes
#include <iostream>
#include<stdio.h>
#include<ctype.h>
#include<stdlib.h>
#include <getopt.h>
#include <cstdlib>
#include <cstdio>
#include <algorithm>

#include "render_particles.h"
#include "visual_sys.h"

/*
 * struct Parms
 * {
 *     float3 L;
 *     int N;
 *     float a0;
 *     float cutoff;
 *     float drive_x;
 *     float drive_y;
 *     float dt;
 *     float T;
 *     int pins_per_cell; //mean number of pins per cell
 *     float pin_force;   //amplitude of the gaussian pin potential
 *     float pin_cutoff;  //interaction range seted to pin_cutoff/5
 * };
 */


Parms parms;

const uint width = 640, height = 480;

ParticleRenderer *renderer = 0;
visual_sys *xy_model = 0;

// view params
int ox, oy;
bool _pause = false;

float camera_trans[] = {0, 0, -parms.L.x};
float camera_rot[]   = {90, 90, 0};
float camera_trans_lag[] = {0, 0, -3};
float camera_rot_lag[] = {0, 0, 0};
const float inertia = 0.1f;

bool displayEnabled = true;


//ParticleRenderer::DisplayMode displayMode = ParticleRenderer::PARTICLE_SPHERES;
ParticleRenderer::DisplayMode displayMode = ParticleRenderer::PARTICLE_SPRITES;
//ParticleRenderer::DisplayMode displayMode = ParticleRenderer::PARTICLE_POINTS;

float modelView[16];

float particleRadius;// = parms.a0/4;

void draw_coordinates()
{
    glBegin(GL_LINES);
    glColor3f(1, 0, 0);
    glVertex3f(0, 0, 0);
    glVertex3f(parms.L.x/2, 0, 0);

    glColor3f(0, 1, 0);
    glVertex3f(0, 0, 0);
    glVertex3f(0, parms.L.x/2, 0);

    glColor3f(0, 0, 1);
    glVertex3f(0, 0, 0);
    glVertex3f(0, 0, parms.L.x/2);

    glEnd();

    glBegin(GL_LINE_LOOP);
    glColor3f(1, 1, 1);
    glVertex3f(-parms.L.y/2, 0, -parms.L.x/2);
    glVertex3f(parms.L.y/2, 0, -parms.L.x/2);
    glVertex3f(parms.L.y/2, 0, parms.L.x/2);
    glVertex3f(-parms.L.y/2, 0, parms.L.x/2);
    glEnd();

}

my_h5 *file = nullptr;

void initGL(int argc, char **argv)
{
    if (!parms.set_by_command_line(argc, argv)) {
        exit (0);
    }
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
    glutInitWindowSize(width, height);
    glutCreateWindow("CUDA Particles");

    glewInit();

    glEnable(GL_DEPTH_TEST);
    glClearColor(0.25, 0.25, 0.25, 1.0);
    //glClearColor(0.0, 0.0, 0.0, 1.0);

    glutReportErrors();

    if (parms.file != "none"){
        my_h5 file(parms.file);
        int index=-1;
        parms.load_from_file(file, index);
        xy_model = new visual_sys(parms, vortex::init_mode::ZIGZAG);
        xy_model->load_state(file, index);
    }else{
        xy_model = new visual_sys(parms);
    }
    //my_h5 file("result_L_64.000000_FP_0.008000_a0_3.000000_T_0.000000_M_1.000000.h5");

    //int index=190;
    //parms.load_from_file(file, index);

    camera_trans[2] = -parms.L.x;
    particleRadius = parms.a0/4;

    //xy_model = new GLsystem(parms);
    //printf("init system\n");
    //xy_model = new visual_sys(parms);
    //xy_model->load_state(file, index);
    //xy_model = new visual_sys(parms, vortex::init_mode::RANDOM);
    //xy_model->update_cells_index();
    xy_model->update_positions_and_colors();
    renderer = new ParticleRenderer;
    renderer->setParticleRadius(particleRadius);
    //LLENAR ACA CON EL BUFFER DE COLOR DEL XY
    renderer->setColorBuffer(xy_model->get_color_buf());
    printf("color buffer setted\n");
        string file_name;
    if (parms.file == "none"){
        file_name = "result_L_"+to_string(parms.L.x/parms.a0)+
                "_FP_"+to_string(parms.pin_force)+
                "_a0_"+to_string(parms.a0)+
                "_T_"+to_string(parms.T)+
                "_M_"+to_string(parms.M)+".h5";
        file = new my_h5(file_name);
    }
}

int paso = 0;

void display()
{
    if (!_pause){
        if(xy_model){
            xy_model->update_positions();
            if (paso%10 == 0) {
                //xy_model->acumulate_sq();
                //xy_model->check_dt();
            }
            //xy_model->update_cells_index();
            xy_model->update_positions_and_colors();
            paso++;
        }
        if (renderer)
        {
            //CALCULAR LAS POSICIONES, PASAR EL BUFFER Y EL NUMERO DE PARTICULAS
            renderer->setVertexBuffer(xy_model->get_current_read_buffer(), xy_model->get_number_of_particles());
        }
    }
    // render
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // view transform
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    for (int c = 0; c < 3; ++c)
    {
        camera_trans_lag[c] += (camera_trans[c] - camera_trans_lag[c]) * inertia;
        camera_rot_lag[c] += (camera_rot[c] - camera_rot_lag[c]) * inertia;
    }

    glTranslatef(camera_trans_lag[0], camera_trans_lag[1], camera_trans_lag[2]);
    glRotatef(camera_rot_lag[0], 1.0, 0.0, 0.0);
    glRotatef(camera_rot_lag[1], 0.0, 1.0, 0.0);

    glGetFloatv(GL_MODELVIEW_MATRIX, modelView);

    //glColor3f(1.0, 1.0, 1.0);
    //draweRect();
    //glutWireCube(parms.Lxy);
    draw_coordinates();
    if (renderer){
        renderer->display(displayMode);
    }
    
    glutSwapBuffers();
    glutReportErrors();

}

void idleFunc(void)
{
    display();
}

void reshape(int w, int h)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0, (float) w / (float) h, 0.1, 4*parms.L.x);

    glMatrixMode(GL_MODELVIEW);
    glViewport(0, 0, w, h);

    if (renderer)
    {
        renderer->setWindowSize(w, h);
        renderer->setFOV(60.0);
    }
}


void mouse(int button, int state, int x, int y){
    if ( (button == GLUT_LEFT_BUTTON) & (state == GLUT_DOWN) ) {
        ox = x; oy = y;
    }
    if ((button == 3) || (button == 4)) // It's a wheel event
    {
        // Each wheel event reports like a button click, GLUT_DOWN then GLUT_UP
        if (state == GLUT_UP) return; // Disregard redundant GLUT_UP events
        switch (button) {
        case 3:
            camera_trans[2] += parms.L.x/20;
            break;
        case 4:
            camera_trans[2] -= parms.L.x/20;
            break;
        }
    }
}

void motion(int x, int y) {
    camera_rot[0] += (y - oy);
    camera_rot[1] += (x - ox);
    ox = x; oy = y;
    glutPostRedisplay();
}

void key(unsigned char key, int /*x*/, int /*y*/)
{
    switch (key)
    {
    case ' ':
        _pause = !_pause;
        break;

    case 'r':
        displayMode = ParticleRenderer::DisplayMode::PARTICLE_SPHERES;
        break;
    case 't':
        displayMode = ParticleRenderer::DisplayMode::PARTICLE_SPRITES;
        break;
    case 'y':
        displayMode = ParticleRenderer::DisplayMode::PARTICLE_POINTS;
        break;
    case 'f':
        particleRadius += 0.1f;
        renderer->setParticleRadius(particleRadius);
        break;
    case 'g':
        particleRadius -= 0.1f;
        renderer->setParticleRadius(particleRadius);
        break;
    case 'x':
        camera_rot[0] = 90;
        camera_rot[1] = 90;
        camera_rot[2] = 0;
        glutPostRedisplay();
        break;
        
    case 'z':
        camera_rot[0] = 0;
        camera_rot[1] = 90;
        camera_rot[2] = 0;
        glutPostRedisplay();
        break;
        
    case 's':
        parms = xy_model->get_parms();
        parms.T -= .0005;
        printf("temperatura = %f\n", parms.T);
        xy_model->set_parms(parms);
        break;
    case 'w':
        parms = xy_model->get_parms();
        parms.T += .0005;
        printf("temperatura = %f\n", parms.T);
        xy_model->set_parms(parms);
        break;
    case 'd':
        parms = xy_model->get_parms();
        parms.drive_x -= .01;
        printf("drive_x = %f\n", parms.drive_x);
        xy_model->set_parms(parms);
        break;
    case 'e':
        parms = xy_model->get_parms();
        parms.drive_x += .01;
        printf("drive_x = %f\n", parms.drive_x);
        xy_model->set_parms(parms);
        break;

    case 'i':
        parms = xy_model->get_parms();
        parms.drive_y += .01;
        printf("drive_y = %f\n", parms.drive_y);
        xy_model->set_parms(parms);
        break;
    case 'k':
        parms = xy_model->get_parms();
        parms.drive_y -= .01;
        printf("drive_y = %f\n", parms.drive_y);
        xy_model->set_parms(parms);
        break;
    case 'p':
        xy_model->print_parms();
        break;
    case 'b':
        xy_model->write_hdf5(*file, 1, 1, 1);
        break;
    }
    glutPostRedisplay();
}

void cleanup()
{
    if (xy_model)
    {
        delete xy_model;
    }

    if (renderer)
    {
        delete renderer;
    }
#ifndef CPU
    cudaDeviceReset();
#endif

}

////////////////////////////////////////////////////////////////////////////////
// Program main
////////////////////////////////////////////////////////////////////////////////
int
main(int argc, char **argv)
{
#if defined(__linux__)
    setenv ("DISPLAY", ":0", 0);
#endif
    printf("NOTE: The CUDA Samples are not meant for performance measurements. Results may vary when GPU Boost is enabled.\n\n");
    initGL(argc, argv);

    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutIdleFunc(idleFunc);
    glutMouseFunc(mouse);
    glutMotionFunc(motion);
    glutKeyboardFunc(key);
    //glutSpecialFunc(special);
    glutCloseFunc(cleanup);
    glutMainLoop();

    return 0;
}
